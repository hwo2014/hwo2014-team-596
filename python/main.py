import json
import socket
import sys


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.current_throttle = 1
        self.ping()

    def on_car_positions(self, data):
        for d in data:
            if d['id']['color'] == self.my_car:
                angle = d['angle']
                pieceIndex = d['piecePosition']['pieceIndex']
                inPieceDistance = d['piecePosition']['inPieceDistance']
                startLaneIndex = d['piecePosition']['lane']['startLaneIndex']
                endLaneIndex = d['piecePosition']['lane']['endLaneIndex']

        self.throttle(0.5)
        self.log.append([angle, pieceIndex, inPieceDistance, startLaneIndex, endLaneIndex])

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        f1 = open('track.csv','w')
        f2 = open('log.csv','w')
        colnames2 = ['length', 'switch', 'radius', 'angle']
        for name in colnames2:
            f1.write(name)
            f1.write(',')
        f1.write('\n')
        for piece in self.track:
            row = [0, 0, 0, 0]
            if 'length' in piece:
                row[0] = piece['length']
            if 'switch' in piece:
                row[1] = 1
            if 'radius' in piece:
                row[2] = piece['radius']
                row[3] = piece['angle']
            for r in row:
                f1.write("%s," % r)
            f1.write('\n')

        colnames = ['angle', 'pieceIndex', 'inPieceDistance', 'startLaneIndex', 'endLaneIndex']
        for name in colnames:
            f2.write(name)
            f2.write(',')
        f2.write('\n')
        for line in self.log:
            for entry in line:
                f2.write("%s," % entry)
            f2.write("\n")
        self.ping()
        f1.close()
        f2.close()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        #parse data and build track map here
        self.track = data['race']['track']['pieces']
        self.ping()

    def on_your_car(self, data):
        self.my_car = data["color"]
        self.log = []
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
